<?php

namespace App\Manager;

use Plugo\Manager\AbstractManager;
use App\Entity\Article;

class ArticleManager extends AbstractManager {

    //  récupération d'une ressource spécifique par son identifiant
    public function find(int $id) {
        return $this->readOne(Article::class, [ 'id' => $id ]);
    }

    // récupération d'une ressource spécifique répondant à un ou plusieurs critères
    public function findOneBy(array $filters) {
        return $this->readOne(Article::class, $filters);
    }

    //  récupération de toutes les ressources
    public function findAll() {
        return $this->readMany(Article::class);
    }

    // récupération de toutes les ressources répondant à un ou plusieurs critères, de les ordonner, limiter leur nombre et décaler le curseur de sélection
    public function findBy(array $filters, array $order = [], int $limit = null, int $offset = null) {
        return $this->readMany(Article::class, $filters, $order, $limit, $offset);
    }

    // créer
    public function add(Article $article) {
        return $this->create(Article::class, [
                'title' => $article->getTitle(),
                'description' => $article->getDescription(),
                'content' => $article->getContent()
            ]
        );
    }

    // modifier
    public function edit(Article $article) {
        return $this->update(Article::class, [
            'title' => $article->getTitle(),
            'description' => $article->getDescription(),
            'content' => $article->getContent()
        ],
            $article->getId()
        );
    }

    // supprimer
    public function remove(Article $article) {
        return $this->delete(Article::class, $article->getId());
    }

}